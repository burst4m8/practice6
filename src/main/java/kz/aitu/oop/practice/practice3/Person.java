package kz.aitu.oop.practice.practice3;

public interface Person {
    String getName();
    void setName(String name);
}
