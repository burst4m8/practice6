package kz.aitu.oop.practice.practice3;

public abstract class Worker implements Employee{
    private String name;

    public Worker(String name){
        setName(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
