package kz.aitu.oop.practice.practice3;

public class FrontSupervisor extends Worker implements FrontDev{
    public FrontSupervisor(String name) {
        super(name);
    }

    public void develop() {
        developFront();
    }

    public void developFront() {
        System.out.println("I am " + getName() +" and I supervising Front-end!");
    }

    public void work() {
        develop();
    }

}
