package kz.aitu.oop.practice.practice3;

import java.util.ArrayList;
import java.util.List;

public class Company {
    private List<Employee> employees = new ArrayList<Employee>();

    public void addEmployee(Employee employee){
        employees.add(employee);
    }

    public void getWorkers(){
        for(Employee emp : employees){
            emp.work();
        }
    }
}
