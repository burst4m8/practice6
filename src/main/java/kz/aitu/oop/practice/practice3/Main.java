package kz.aitu.oop.practice.practice3;

public class Main {
    public static void main(String[] args){
        Company comp1 = new Company();
        comp1.addEmployee(new FullStackDev("Adilkhan"));
        comp1.addEmployee(new FrontSupervisor("Akan"));
        comp1.addEmployee(new FrontSupervisor("Ulan"));
        comp1.addEmployee(new FullStackDev("Rakhat"));
        comp1.getWorkers();
    }
}
