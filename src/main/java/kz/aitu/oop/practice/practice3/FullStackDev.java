package kz.aitu.oop.practice.practice3;

public class FullStackDev extends Worker implements FrontDev, BackDev {

    public FullStackDev(String name) {
        super(name);
    }

    public void develop() {
        System.out.println("I am " + getName());
        developFront();
        developBack();
    }

    public void developFront() {
        System.out.println("Front-end developer");
    }

    public void developBack() {
        System.out.println("Back-end developer");
    }

    public void work() {
        develop();
    }



}
